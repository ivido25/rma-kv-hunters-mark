package hr.ferit.ivido.huntersmark;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {

    private ImageButton buttonMap, button2;
    private ListView lvMark;
    private ArrayList<MarkerEntry> mMarkerEntries;
    private MarkerAdapter mMarkerAdapter;
    private DataBaseHelper mDataBaseHelper;

    static final int REQUEST_TAKE_PHOTO = 1;
    String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.mDataBaseHelper = mDataBaseHelper.getInstance(this);
        this.mMarkerEntries = mDataBaseHelper.retrieveMyEntries();
        this.mMarkerAdapter = new MarkerAdapter(this.mMarkerEntries);
        this.lvMark.setAdapter(this.mMarkerAdapter);
    }

    private void initUI() {
        this.mDataBaseHelper = mDataBaseHelper.getInstance(this);

        this.buttonMap = (ImageButton)findViewById(R.id.buttonMap);
        this.button2 = (ImageButton)findViewById(R.id.button2);
        this.lvMark = (ListView)findViewById(R.id.lvEntries);

        buttonMap.setImageResource(R.mipmap.maps);
        button2.setImageResource(R.mipmap.camera);

        buttonMap.setOnClickListener(this);
        button2.setOnClickListener(this);
        lvMark.setOnItemLongClickListener(this);
        lvMark.setOnItemClickListener(this);

        this.mMarkerEntries = mDataBaseHelper.retrieveMyEntries();
        this.mMarkerAdapter = new MarkerAdapter(this.mMarkerEntries);
        this.lvMark.setAdapter(this.mMarkerAdapter);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case(R.id.buttonMap):{
                Intent intent = new Intent(this, MapsActivity.class);
                startActivity(intent);
            }
            break;
            case(R.id.button2):
            {
                dispatchTakePictureIntent();
            }
            break;

        }
    }
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        final int lPosition = position;
        final AdapterView<?> lParent = parent;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.delete_msg_title).setMessage(R.string.delete_msg_text);
        alertDialogBuilder
                .setCancelable(true)
                .setPositiveButton(R.string.delete_msg_positive, new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int i){
                        MarkerAdapter adapter = (MarkerAdapter) lParent.getAdapter();
                        MarkerEntry selectedTask = (MarkerEntry) adapter.getItem(lPosition);
                        adapter.remove(lPosition);
                        mDataBaseHelper.deleteTask(selectedTask);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        MarkerAdapter adapter = (MarkerAdapter) adapterView.getAdapter();
        MarkerEntry selectedTask = (MarkerEntry) adapter.getItem(position);
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra(getResources().getString(R.string.intent_key_lat), (float) selectedTask.mLatitude);
        intent.putExtra(getResources().getString(R.string.intent_key_lng), (float) selectedTask.mLongitude);
        startActivity(intent);
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

}
