package hr.ferit.ivido.huntersmark;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapLongClickListener {

    private GoogleMap gmap;
    private final static int MY_PERMISSION_FINE_LOCATION = 1;
    private String[] spinnerArr = new String[] {"Marker Type", "Wild game", "Feeder", "Trap"};

    private ArrayList<MarkerEntry> mMarkerEntries;
    private MarkerAdapter mMarkerAdapter;
    private DataBaseHelper mDataBaseHelper;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        this.mDataBaseHelper = mDataBaseHelper.getInstance(this);
        this.mMarkerEntries = mDataBaseHelper.retrieveMyEntries();
        extras = getIntent().getExtras();
        setUpMap();
    }

    private void setUpMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap = googleMap;
        gmap.setOnMapLongClickListener(this);
        gmap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        gmap.clear();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                gmap.setMyLocationEnabled(true);
        }
        else {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                try {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                        new AlertDialog.Builder(this)
                                .setTitle(R.string.title_location_permission)
                                .setMessage(R.string.text_location_permission)
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        ActivityCompat.requestPermissions(MapsActivity.this,
                                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                                MY_PERMISSION_FINE_LOCATION);
                                    }
                                })
                                .create()
                                .show();
                    } else {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                MY_PERMISSION_FINE_LOCATION);
                    }
                }catch(Exception e ) {
                }
            }

        }
        if(mMarkerEntries.size() != 0){
            addMarkers();
        }
        MapStateManager mgr = new MapStateManager(this);
        CameraPosition position = mgr.getSavedCameraPosition();

        if(extras != null){
            LatLng latLngExtras = new LatLng(
                    extras.getFloat(getResources().getString(R.string.intent_key_lat)),
                    extras.getFloat(getResources().getString(R.string.intent_key_lng))
            );
            gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngExtras,position.zoom));
        }
        else{
            if(position != null){
                CameraUpdate update = CameraUpdateFactory.newCameraPosition(position);
                gmap.moveCamera(update);
            }
            else{
                gmap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(45,18)));
            }
        }
    }

    private void addMarkers() {
        Integer i = 0;
        MarkerEntry entry;
        do {
            MarkerOptions mOptions = new MarkerOptions();
            entry = mMarkerEntries.get(i);
            mOptions = makeMarkerOptions(mOptions, entry.getmTitle().toString());
            gmap.addMarker(mOptions
            .position(entry.getmLatLng())
            .snippet(entry.getmSnippet())
            .title(entry.getmTitle())
            );
            i++;
        } while(i < mMarkerEntries.size());
    }

    @Override
    protected void onPause() {
        super.onPause();
        MapStateManager mgr = new MapStateManager(this);
        mgr.saveMapState(gmap);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.mDataBaseHelper = mDataBaseHelper.getInstance(this);
        this.mMarkerEntries = mDataBaseHelper.retrieveMyEntries();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case MY_PERMISSION_FINE_LOCATION:
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        gmap.setMyLocationEnabled(true);
                        this.recreate();
                    }
                }else{
                    gmap.setMyLocationEnabled(false);
                }break;
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {

        final LatLng ll = latLng;

        LayoutInflater li = LayoutInflater.from(this);
        View dialogView = li.inflate(R.layout.custom_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.entry);
        alertDialogBuilder.setView(dialogView);

        final EditText userInput = (EditText) dialogView.findViewById(R.id.et_input);
        final Spinner spinner = (Spinner)dialogView.findViewById(R.id.spinner);
        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(
                dialogView.getContext(),
                android.R.layout.simple_spinner_item,
                spinnerArr);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(spinnerAdapter);
            spinnerAdapter.notifyDataSetChanged();

        alertDialogBuilder
                .setCancelable(true)
                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                final MarkerOptions mOptions = new MarkerOptions();
                                makeMarkerOptions(mOptions, spinner.getSelectedItem().toString());
                                gmap.addMarker(mOptions
                                        .position(ll)
                                        .title(spinner.getSelectedItem().toString())
                                        .snippet(userInput.getText().toString())
                                );
                                MarkerEntry entry = new MarkerEntry(
                                        spinner.getSelectedItem().toString(),
                                        userInput.getText().toString(),
                                        mMarkerEntries.size(),
                                        ll.latitude,
                                        ll.longitude
                                );
                                mDataBaseHelper.insertTask(entry);
                                mMarkerEntries.add(entry);
                            }
                        })
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        }

    private MarkerOptions makeMarkerOptions(MarkerOptions mOptions, String type) {
        MarkerOptions resOptions = mOptions;
        switch(type){
            case "Trap":{
                resOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.trap));
                break;
            }
            case "Feeder":{
                resOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.feeder));
                break;
            }
            case "Wild game":{
                resOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.wild_game));
                break;
            }
            default:{
                break;
            }
        }
        return resOptions;
    }
}
