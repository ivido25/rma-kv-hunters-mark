package hr.ferit.ivido.huntersmark;

import com.google.android.gms.maps.model.LatLng;

public class MarkerEntry {
    private String mTitle;
    private String mSnippet;
    private int mCategory;
    double mLatitude, mLongitude;

    public MarkerEntry(String mTitle, String mSnippet,int mCategory, double mLatitude, double mLongitude ){
        this. mTitle = mTitle;
        this. mSnippet = mSnippet;
        this.mCategory = mCategory;
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmSnippet() {
        return mSnippet;
    }

    public LatLng getmLatLng() {
        LatLng mLatLng = new LatLng(mLatitude,mLongitude);
        return mLatLng;
    }

    public double getmLatitude() {
        return mLatitude;
    }

    public double getmLongitude() {
        return mLongitude;
    }

    public int getmCategory(){
        return mCategory;
    }

    public void setmCategory(int mCategory){
        this.mCategory = mCategory;
    }
}
