package hr.ferit.ivido.huntersmark;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.util.ArrayList;

public class MarkerAdapter extends BaseAdapter{

    ArrayList<MarkerEntry> mMarkerEntries;

    public MarkerAdapter(ArrayList<MarkerEntry> mMarkerEntries){this.mMarkerEntries=mMarkerEntries;}

    @Override
    public int getCount() {
        return this.mMarkerEntries.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mMarkerEntries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TaskViewHolder taskViewHolder;
        Context parentContext = parent.getContext();

        if(null==convertView)
        {
            convertView=View.inflate(parentContext,R.layout.item_marker_entry,null);

            taskViewHolder=new TaskViewHolder();
            taskViewHolder.tvSnippet=(TextView) convertView.findViewById(R.id.tvSnippet);
            taskViewHolder.tvTitle=(TextView) convertView.findViewById(R.id.tvTitle);
            taskViewHolder.imIcon = (ImageView) convertView.findViewById(R.id.imIcon);

            convertView.setTag(taskViewHolder);
        }
        else{
            taskViewHolder =(TaskViewHolder) convertView.getTag();
        }
        MarkerEntry currentTask = this.mMarkerEntries.get(position);
        Integer index = position +1;
        taskViewHolder.tvSnippet.setText(currentTask.getmSnippet());
        taskViewHolder.tvTitle.setText(index.toString() +". "+ currentTask.getmTitle());
        switch (currentTask.getmTitle()){
            case "Wild game":{
                taskViewHolder.imIcon.setImageResource(R.mipmap.wild_game);
                break;
            }
            case "Trap":{
                taskViewHolder.imIcon.setImageResource(R.mipmap.trap);
                break;
            }
            case "Feeder":{
                taskViewHolder.imIcon.setImageResource(R.mipmap.feeder);
                break;
            }
            default:{
                taskViewHolder.imIcon.setImageDrawable(null);
            }
        }

        return convertView;
    }

    public void add(MarkerEntry task) {
        this.mMarkerEntries.add(0, task);
        this.notifyDataSetChanged();
    }

    public void remove(int position) {
        this.mMarkerEntries.remove(position);
        this.notifyDataSetChanged();
    }

    static class TaskViewHolder
    {
        private TextView tvTitle;
        private TextView tvSnippet;
        private ImageView imIcon;
    }
}
