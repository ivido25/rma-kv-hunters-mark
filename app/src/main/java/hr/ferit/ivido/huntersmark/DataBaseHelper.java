package hr.ferit.ivido.huntersmark;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final int SCHEMA             = 1;
    private static final String DATABASE_NAME   = "wildGame.db";


    static final String TABLE_MARKERS   = "markers";
    static final String CATEGORY        = "category";
    static final String LATITUDE        = "latitude";
    static final String LONGITUDE       = "longitude";
    static final String SNIPPET         = "snippet";
    static final String TITLE           = "title";


    private static DataBaseHelper mInstance = null;

    private DataBaseHelper (Context context)
    {
        super(context,DATABASE_NAME,null,SCHEMA);
    }

    public static synchronized DataBaseHelper getInstance(Context context)
    {
        if(null == mInstance)
        {
            context = context.getApplicationContext();
            mInstance = new DataBaseHelper(context);
        }
        return  mInstance;
    }
    @Override
    public void onCreate(SQLiteDatabase db){
        final String CREATE_TABLE_MY_MARKERS =
                "CREATE TABLE "+TABLE_MARKERS+
                        " ("+TITLE+" TEXT,"+SNIPPET+" TEXT,"+CATEGORY+" INTEGER, "+LATITUDE+" FLOAT, "+LONGITUDE+" FLOAT);";
        db.execSQL(CREATE_TABLE_MY_MARKERS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        final String DROP_TABLE_MY_MARKERS = "DROP TABLE IF EXISTS "+TABLE_MARKERS;
        db.execSQL(DROP_TABLE_MY_MARKERS);
        onCreate(db);
    }

    public ArrayList<MarkerEntry> retrieveMyEntries()
    {
        // Usage of a raw query:
        String SELECT_ALL_TASKS = "SELECT "+TITLE+","
                +SNIPPET+","+CATEGORY+","+LATITUDE+","+LONGITUDE+" FROM "+TABLE_MARKERS;
        ArrayList<MarkerEntry> mMarkerEntries = new ArrayList<>();
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor mEntriesCursor = database.rawQuery(SELECT_ALL_TASKS, null);
        if(mEntriesCursor.moveToFirst())
        {
            do {
                MarkerEntry entry = new MarkerEntry(
                        mEntriesCursor.getString(0),
                        mEntriesCursor.getString(1),
                        mEntriesCursor.getInt(2),
                        mEntriesCursor.getFloat(3),
                        mEntriesCursor.getFloat(4)
                );
                mMarkerEntries.add(0,entry);
            }while(mEntriesCursor.moveToNext());
        }
        mEntriesCursor.close();
        database.close();
        return mMarkerEntries;
    }

    public void insertTask(MarkerEntry entry) {
        //Using the insert method of the database object:
        ContentValues markerValues = new ContentValues();
        markerValues.put(TITLE,entry.getmTitle());
        markerValues.put(SNIPPET,entry.getmSnippet());
        markerValues.put(CATEGORY,entry.getmCategory());
        markerValues.put(LATITUDE,entry.getmLatitude());
        markerValues.put(LONGITUDE,entry.getmLongitude());

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_MARKERS, TITLE, markerValues);
        db.close();
    }
    public void deleteTask(MarkerEntry entry){

        String name = entry.getmTitle();
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MARKERS, TITLE + "='" + name+"'", null);
    }
}
